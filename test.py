"""
このファイルに解答コードを書いてください
"""
pair_dict = {}
m = 0

with open('input.txt', 'r') as f:
    for line in f:
        pair = line.split(':')

        if len(pair) == 1:
            m = int(pair[0])
            break

        pair_dict[int(pair[0])] = pair[1].rstrip()

res = ''
for i, s in sorted(pair_dict.items()):
    if m % i == 0:
        res += s

res = m if res == '' else res
print(res)